source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'

# Ruby ODM for MongoDB
gem "mongoid", "~> 4.0.0"

# Use mysql as the database for Active Record
gem 'mysql2'

# Assets
gem 'slim', '~> 3.0.0'
gem 'sass-rails', '~> 4.0.0'
gem 'uglifier', '>= 1.3.0' # Use Uglifier as compressor for JavaScript assets
gem 'coffee-rails', '~> 4.0.0'
gem 'bootstrap-sass', '~> 3.0.2.0'
gem 'therubyracer', platforms: :ruby
gem 'jquery-rails'
gem 'react-rails', '~> 1.0'
gem 'react-bootstrap-rails'
gem "browserify-rails", "~> 0.7"
gem "autoprefixer-rails"
gem "momentjs-rails"


gem "brakeman", :require => false
# gem 'jwplayer-rails'

gem 'evil-blocks-rails'
gem 'gon'
gem 'carrierwave'
gem 'carrierwave-ftp', :require => 'carrierwave/storage/sftp' # SFTP only
gem 'mini_magick'

gem 'enumerize'

gem "jquery-tmpl-rails"
gem 'rack-cors', :require => 'rack/cors'

gem 'd3-rails'

# SWF files of Flash player
gem "non-stupid-digest-assets"

# Localization
gem 'i18n'
gem "i18n-js", ">= 3.0.0.rc8"

# Settings
gem 'rails_config'
gem 'faraday', '0.9.1'

# Responders
gem 'responders'

# Decorators
gem 'draper'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# Search
gem 'chewy', '0.10.1'#, :git => 'https://github.com/toptal/chewy.git'
gem 'chewy_kiqqer'

gem 'nokogiri'

# Paginator
gem 'kaminari'

# Email
gem 'mandrill_mailer'

gem 'active_data'
gem 'wannabe_bool'
gem 'virtus'
gem 'cancancan'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

gem 'paperclip', '~> 4.3'
gem 'activerecord-mysql-unsigned'
gem 'composite_primary_keys', '~> 8.x'

gem 'google-cloud-translate'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

# Colorize console output
gem 'colorize', require: false
# gem 'honeybadger', '~> 2.0'
gem 'newrelic_rpm'

gem 'sidekiq'
gem 'sidekiq-status'
gem 'sidekiq-unique-jobs'
gem 'sidekiq-limit_fetch'
gem 'sidekiq-client-cli'
gem 'hitimes', '1.2.4'

gem 'sinatra', require: false
gem 'axlsx_rails'

gem 'whenever', require: false

gem 'capistrano-ci'
gem 'maxminddb'
gem 'ransack'

# for translation_modes
gem 'request_store'

gem 'lhm'
gem 'rack-mini-profiler', require: false
gem 'daemons', require: false
gem 'statsd-ruby', require: false
gem 'raindrops', '~>0.16.0'
gem 'active_model_serializers'
gem 'useragent'

group :deploy do
  gem 'capistrano', '2.15.5'
  gem 'rvm-capistrano'
  gem 'capistrano-ext'
  gem 'capistrano-npm'
  gem 'capistrano-recipes', github: 'glyuck/capistrano-recipes'
  gem 'capistrano_colors'
  gem 'capistrano-sidekiq'
  gem 'capistrano-slack', git: 'https://github.com/j-mcnally/capistrano-slack.git'
end

group :production, :staging do
  gem 'unicorn', '~> 5.3.0'
  gem 'rails_12factor'
  gem 'exception_notification'
  gem 'slack-notifier'
end

group :development do
  gem 'unicorn-rails'
  gem 'capistrano-unicorn', :require => false
  # For view trace errors
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'rails-dev-tweaks'
  gem 'spring'
  gem 'meta_request'

  gem 'letter_opener'

  gem 'quiet_assets'
  gem 'awesome_print'
  gem 'overcommit', require: false
end

group :test, :development do
  gem 'byebug'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'rspec-console'
  gem 'shoulda'
  gem 'factory_girl_rails'
  gem 'factory_girl-seeds'
  gem 'database_cleaner'
  gem 'fakeweb'
  gem 'ffaker'
  gem 'pry'
  gem 'pry-developer_tools'
  gem 'pry-doc'
  gem 'pry-docmore'
  gem 'pry-git'
  gem 'pry-highlight'
  gem 'pry-nav'
  gem 'pry-pretty-numeric'
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'pry-stack_explorer'
  gem 'pry-theme'
  gem 'jasmine'
  gem 'jasmine-jquery-rails'
end

group :test do
  gem 'rspec-sidekiq'
  gem 'mock_redis'
  gem 'webmock'
end

group :test do
  gem "json-schema"
end
