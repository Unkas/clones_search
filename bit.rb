require 'fileutils'

PATH_TO_FILES = Dir.pwd + '/files/'

def self.short path_to_file
  path_to_file.gsub(PATH_TO_FILES, '')
end

all_files_arr = []
clones_arr = []
files_path = PATH_TO_FILES + '*'

Dir[files_path].each do |filename|
  size = File.size(filename)
  hash = {name: filename, size: size}
  similar_file = all_files_arr.select {|file| file[:size] == size }
  unless similar_file.empty?
    similar_file.each do |similar_file|
      similar_file_name = similar_file[:name]
      if FileUtils.compare_file(similar_file_name, filename)
        clone_room = clones_arr.select{|(x, y)| x == short(similar_file_name)}
        if clone_room.empty? #если первый клон отсутствует в реестре
          clones_arr << [short(similar_file_name), short(filename)]
        else                 #если уже присутствует
          index = clones_arr.index(clone_room[0])
          clones_arr[index] << short(filename)
        end
      end
    end
  end
  all_files_arr << hash
end
clones_arr = clones_arr.map{|x| x.uniq}
puts clones_arr.to_s
